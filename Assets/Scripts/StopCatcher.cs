﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GoldfishScoping {
    public class StopCatcher : MonoBehaviour {

        private void OnTriggerStay (Collider other) {
            if (other.GetComponentInParent<CatcherManager>()!=null) {
                var catcher = other.GetComponentInParent<CatcherManager> ();
                catcher.isStopped = true;
            }
        }
        private void OnTriggerExit (Collider other) {
            if (other.GetComponentInParent<CatcherManager> () != null) {
                var catcher = other.GetComponentInParent<CatcherManager> ();
                catcher.isStopped = false;
            }
        }
    }
}
