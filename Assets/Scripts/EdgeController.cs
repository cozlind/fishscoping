﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoldfishScoping {
    public class EdgeController : MonoBehaviour {

        void OnCollisionStay (Collision collision) {
            var obj = collision.gameObject;
            if (obj.CompareTag ("Fish")&& obj.transform.position.y>9.2&& obj.GetComponent<Fish>().state==FishState.Normal) {
                obj.transform.Translate (-2*obj.transform.forward);
            }
        }
    }
}