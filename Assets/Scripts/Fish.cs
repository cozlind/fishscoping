﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GoldfishScoping {
    [RequireComponent (typeof (FishSteering))]
    public class Fish : MonoBehaviour {
        [Range(2,15)]
        public int score;
        public FishState state;
        public StateMachine<Fish> stateMachine;
        [HideInInspector]
        public FishSteering fishSteering;
        private void Awake () {
            stateMachine = new StateMachine<Fish> (this);
            fishSteering = GetComponent<FishSteering> ();
            stateMachine.currentState = FishIdle.Instance;
            stateMachine.currentState.Enter (this);
            stateMachine.globalState = FishGlobalState.Instance;
            stateMachine.globalState.Enter (this);
        }
        private void FixedUpdate () {
            stateMachine.Update ();
            if (!Params.Instance.bowlDetect.bounds.Contains (transform.position)&&state==FishState.InBowl) {
                state = FishState.Normal;
            }
        }

    }
    public enum FishState { Normal, Catched, InBowl};

    public class FishGlobalState : State<Fish> {
        private static FishGlobalState instance;
        protected FishGlobalState () {
            if (instance != null) return;
            instance = this;
        }
        public static new FishGlobalState Instance {
            get {
                if (instance == null) new FishGlobalState ();
                return instance;
            }
        }

        public override void Enter (Fish fish) {

        }
        public override void Execute (Fish fish) {
        }
        public override void Exit (Fish fish) {

        }
        public override bool OnMessage (Fish fish) {
            return false;
        }
    }
    public class FishIdle : State<Fish> {
        private static FishIdle instance;
        private float continueTime;
        protected FishIdle () {
            if (instance != null) return;
            instance = this;
        }
        public static new FishIdle Instance {
            get {
                if (instance == null) new FishIdle ();
                return instance;
            }
        }
        public override void Execute (Fish fish) {
                fish.stateMachine.ChangeState (FishSwim.Instance);
        }
    }


    public class FishSwim : State<Fish> {
        private static FishSwim instance;
        private float continueTime;
        protected FishSwim () {
            if (instance != null) return;
            instance = this;
        }
        public static new FishSwim Instance {
            get {
                if (instance == null) new FishSwim ();
                return instance;
            }
        }
        public override void Enter (Fish fish) {
            continueTime = Time.time + Random.Range (0.0f, Params.Instance.swimTime);
            Params.Instance.wanderEnabled = true;
            Params.Instance.wallAvoidanceEnabled = true;
        }
        public override void Execute (Fish fish) {
            fish.fishSteering.Execute (fish.state);

            if (fish.state==FishState.Catched)
                fish.stateMachine.ChangeState (FishCatched.Instance);

            if (Time.time > continueTime)
                fish.stateMachine.ChangeState (FishIdle.Instance);

        }
    }

    public class FishCatched : State<Fish> {
        private static FishCatched instance;
        public Transform catcher;
        private float time;
        protected FishCatched () {
            if (instance != null) return;
            instance = this;
        }
        public static new FishCatched Instance {
            get {
                if (instance == null) new FishCatched ();
                return instance;
            }
        }
        public override void Enter (Fish fish) {
            time = Time.time;
        }
        public override void Execute (Fish fish) {
            if (Time.time - time > 5) {
                fish.state = FishState.Normal;
                fish.stateMachine.ChangeState (FishSwim.Instance);
            }
            fish.fishSteering.Execute (fish.state);
            if (fish.state == FishState.InBowl)
                fish.stateMachine.ChangeState (FishInBowl.Instance);
        }
        public override void Exit (Fish fish) {

        }
        public override bool OnMessage (Fish fish) {
            return false;
        }
    }
    public class FishInBowl : State<Fish> {
        private static FishInBowl instance;
        private Transform catcher;
        protected FishInBowl () {
            if (instance != null) return;
            instance = this;
        }
        public static new FishInBowl Instance {
            get {
                if (instance == null) new FishInBowl ();
                return instance;
            }
        }
        public override void Enter (Fish fish) {
            catcher = GameObject.FindObjectOfType<CatcherManager> ().transform;
        }
        public override void Execute (Fish fish) {
            fish.fishSteering.Execute (fish.state);
            fish.transform.rotation = Quaternion.identity;
            if (fish.state == FishState.Normal)
                fish.stateMachine.ChangeState (FishSwim.Instance);
        }
        public override void Exit (Fish fish) {

        }
        public override bool OnMessage (Fish fish) {
            return false;
        }
    }
}