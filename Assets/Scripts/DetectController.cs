﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoldfishScoping {
    public class DetectController : MonoBehaviour {

        public Fish currFish;
        public FishState state;
        private KinGyoScenario scenario;
        public string type;

        private void Awake () {
            currFish = null;
            scenario = GameObject.FindObjectOfType<KinGyoScenario> ();
        }
        private void OnTriggerEnter (Collider other) {
            if (!other.isTrigger && other.gameObject.CompareTag ("FishBody")) {
                currFish = other.GetComponentInParent<Fish> ();
                currFish.state = state;
                if (state == FishState.InBowl) {
                    scenario.UpdateScore ();
                    scenario.InBowlAudio ();
                }
            }
        }
        private void OnTriggerStay (Collider other) {
            if (!other.isTrigger && other.gameObject.CompareTag ("FishBody")) {
                currFish = other.GetComponentInParent<Fish> ();
                currFish.state = state;
            }
        }
        private void OnTriggerExit (Collider other) {
            if (!other.isTrigger && other.gameObject.CompareTag ("FishBody")) {
                Debug.Log (other.transform.parent.name);
                currFish.state = FishState.Normal;
                scenario.UpdateScore ();
                currFish = null;
            }
        }
    }
}
