﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GoldfishScoping {
    public class KinGyoScenario : MonoBehaviour {
        [Header ("Process")]
        public float[] time;
        private int timeIndex = 1;
        private float currentTime = 0;
        private bool isSuccess = false;
        public int score = 0;
        public float poiValue = 0;
        private bool isPoiReady = true;
        [Header ("Audio")]
        public AudioClip[] audios;
        AudioSource audio;
        public AudioSource bgm;
        [Header ("GameObject")]
        public CatcherManager catcher;
        public GameObject redPfb;
        public GameObject yellowPfb;
        public GameObject blackPfb;
        public GameObject redWhitePfb;
        public GameObject brokenPoi;
        public GameObject poi;
        public GameObject[] pois;
        private List<GameObject> fishes;
        public int poiIndex = 1;
        public int fishNum = 4;
        [Header ("UI")]
        public Text fishNumUI;
        public Text fishNumUI2;
        public Text timeUI;
        public Slider speedSlider;
        public Slider timeSlider;
        private float timeValue;
        public GameObject titleUI;
        public GameObject replayUI;
        public GameObject settingUI;
        public Transform poiStamina;




        private static KinGyoScenario instance;
        public static KinGyoScenario Instance {
            get {
                return instance;
            }
        }
        private void Awake () {
            instance = this;
            audio = GetComponent<AudioSource> ();
            currentTime = 0;
            catcher.isActive = false;
            replayUI.SetActive (false);
            settingUI.SetActive (false);
            titleUI.SetActive (true);

            fishes = new List<GameObject> ();
            for (int i = 0; i < fishNum; i++) {
                fishes.Add (Instantiate (redPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (yellowPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (blackPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (redWhitePfb, transform.position, Quaternion.identity));
            }
#if UNITY_EDITOR
            //startPlay ();
            bgm.volume = 0;
#endif
        }
        private void Ready () {

            poiValue = 0;
            isPoiReady = true;
            catcher.isActive=true;
            catcher.transform.position = new Vector3 (-43, -2f, 22);
            bgm.pitch = 1;
            poiIndex = 1;
            timeUI.fontSize = 190;
            currentTime = 0;
            titleUI.SetActive (false);
            replayUI.SetActive (false);
            settingUI.SetActive (false);
        }
        public void startPlay () {

            Ready ();

            timeIndex = 2;

            foreach (var p in pois) {
                p.SetActive (true);
            }
            brokenPoi.SetActive (false);
            audio.clip = audios[0];
            audio.Play ();


            for (int i = fishes.Count - 1; i >= 0; i--) {
                Destroy (fishes[i]);
            }
            fishes.Clear ();
            for (int i = 0; i < fishNum; i++) {
                fishes.Add (Instantiate (redPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (yellowPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (blackPfb, transform.position, Quaternion.identity));
                fishes.Add (Instantiate (redWhitePfb, transform.position, Quaternion.identity));
            }
        }
        public void endPlay () {
            audio.clip = audios[0];
            audio.Play ();
            Application.Quit ();
        }
        public void NewPoi () {
            if (poiIndex < 0) {
                //poi run out
                Ready ();
                timeIndex--;
                audio.clip = audios[4];
                audio.Play ();
                replayUI.SetActive (true);
                return;
            }
            if (timeIndex != 2) return;
            pois[poiIndex--].SetActive (false);
            poi.SetActive (true);
            brokenPoi.SetActive (false);
            poiValue = 0;
            isPoiReady = true;
        }
        
        public void UpdateScore () {

            int num = 0;
            for (int i = fishes.Count - 1; i >= 0; i--) {
                var f = fishes[i].GetComponent<Fish> ();
                if (f.state == FishState.InBowl) {
                    num+=f.score;
                }
            }
            fishNumUI.text = num.ToString ();
        }
        private GameObject PreviousUI;
        public void OpenSettingUI () {
            PreviousUI = titleUI.activeSelf ? titleUI : replayUI;
            Ready ();
            settingUI.SetActive (true);
        }
        public void BackUI () {
            Debug.Log (speedSlider.value+":"+timeSlider.value);
            Params.Instance.speedFactor = speedSlider.value;
            time[2] =60+30*timeSlider.value;
            Ready ();
            PreviousUI.SetActive (true);
        }
        public void InBowlAudio () {
            audio.clip = audios[1];
            audio.Play ();
        }

        private void FixedUpdate () {
            //quit
            if (Input.GetKeyDown (KeyCode.Escape)) Application.Quit ();


            UpdateScore ();


            poiStamina.localScale = new Vector3 (poiStamina.localScale.x, Mathf.Lerp (0.115f, 0.001f, poiValue), poiStamina.localScale.z);
            poiStamina.localPosition = new Vector3 (poiStamina.localPosition.x, Mathf.Lerp (-0.113f, -0.17f, poiValue), poiStamina.localPosition.z);

            currentTime += Time.fixedDeltaTime;
            switch (timeIndex) {
                case 0:
                    if (time[timeIndex] > currentTime) {
                        //titleUI.GetComponent<Image> ().color = Color.Lerp (Color.black, Color.white, currentTime / time[timeIndex]);
                        Camera.main.fieldOfView = Mathf.Lerp (35, 24, currentTime / time[timeIndex]);
                    } else {
                        timeIndex++;
                        currentTime = 0;
                    }
                    break;
                case 1:
                    currentTime = 0;
                    break;
                case 2:
                        Camera.main.fieldOfView = Mathf.Lerp (35, 24, currentTime / time[0]);
                        if (time[timeIndex] > currentTime) {
                        int t = Mathf.RoundToInt (time[timeIndex] - currentTime);
                        timeUI.text = t.ToString ();

                        //final 5 seconds
                        if (t <= 10) {
                            timeUI.fontSize = 300;
                            bgm.pitch = 1.7f;
                        }

                        //poi stamina
                        if (catcher.isUsePoi) {
                            poiValue += Time.fixedDeltaTime / 24;
                            if (poiValue >= 1&& isPoiReady) {
                                //poi stamina run out
                                audio.clip = audios[4];
                                audio.Play ();
                                poi.SetActive (false);
                                brokenPoi.SetActive (true);
                                isPoiReady = false;
                                Invoke ("NewPoi", 3);
                            }
                        }

                    } else {
                        //time out
                        Ready ();
                        timeIndex--;
                        replayUI.SetActive (true);
                        fishNumUI2.text = fishNumUI.text + "点";
                        audio.clip = audios[3];
                        audio.Play ();
                    }
                    break;
            }
        }
    }
}
