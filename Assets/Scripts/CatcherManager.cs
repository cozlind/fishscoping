﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoldfishScoping {
    public class CatcherManager : ControllerManager {


        public bool isConnected = false;
        public bool isActive = false;
        public bool isScoping = false;
        public bool isUsePoi = false;
        public bool isFalling = false;
        public bool isStopped = false;
        public float gravity = 60;
        public float moveSpeed=2;
        public float upSpeed = 4;
        public float catchHeight = 21;
        public float generalHeight = -2f;
        public float Pitch, Roll;
        private Rigidbody rb;
        private Vector3 rot;
        private float originalMoveSpeed;
        public Vector3 v;
        public BoxCollider collider;
        public DetectController poi;
        
        private void Start () {
            originalMoveSpeed = moveSpeed;
            rb = GetComponent<Rigidbody> ();
            resetPos = transform.position;
            if (isConnected) {
                Invoke ("Calibration", 0.9f);
            }
        }
        void Calibration () {
            benchmark.Set (pitch, 0, roll);
        }
        void FixedUpdate () {
            if (isConnected) {
                UpdateData ();

            }
            if (!isActive) return;

            rot.Set (-eulerAngles.x, 0, eulerAngles.z);
            transform.eulerAngles = rot;

            roll = Mathf.Abs (roll - benchmark.z) < 4 ? 0 : roll - benchmark.z;
            pitch = Mathf.Abs (pitch - benchmark.x) < 4 ? 0 : pitch - benchmark.x;
            //Debug.Log (roll+":"+ pitch);

            if (!isConnected) {
                roll = Roll;
                pitch = Pitch;
                rot.Set (roll, 0, pitch);
                transform.eulerAngles = rot;
            }

            //move
            v = new Vector3 (-roll * moveSpeed, 0, -pitch * moveSpeed );
            bool isUnderHeight = transform.position.y < catchHeight;
            bool isThumbPressed = (fr1 > 1 || fl1 > 1);
            bool isForefingerPressed = (fr2 > 1 || fl2 > 1);
            if (!isFalling) {
                if (isUnderHeight) {//under the height
                    if (isThumbPressed)
                        v = Vector3.zero;// press thumb to stop moving
                    if (isForefingerPressed) {//press forefinger to move upwards
                        v = Vector3.up * upSpeed ;
                        isUsePoi = true;
                    } else if(transform.position.y > generalHeight) {//loose the poi during upward move
                        isUsePoi = false;
                        isFalling = true;
                    }
                } else {//above the height
                    v += new Vector3 (0.1f * roll, 0, 0.1f * pitch);
                    if (isStopped) v = Vector3.zero;
                    if (!isForefingerPressed) {
                        isUsePoi = false;
                        isFalling = true;
                    }
                }
            } else {
                v= Vector3.down* gravity;
                if ((transform.position + v * Time.fixedDeltaTime).y <= generalHeight)
                    isFalling = false;
            }
            if (collider.bounds.Contains (transform.position + v * Time.fixedDeltaTime))
                transform.position += v * Time.fixedDeltaTime;
            else if (collider.bounds.Contains (transform.position + Vector3.right*v.x * Time.fixedDeltaTime))
                transform.position += Vector3.right * v.x * Time.fixedDeltaTime;
            else if (collider.bounds.Contains (transform.position + Vector3.forward * v.z * Time.fixedDeltaTime))
                transform.position += Vector3.forward * v.z * Time.fixedDeltaTime;
        }
    }
}
