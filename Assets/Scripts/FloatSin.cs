﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatSin : MonoBehaviour {

    public float amplification;
    public float frequency;
    private float originalY;
    private void Start () {
        originalY = transform.position.y;
    }
    void Update () {
        Vector3 pos = transform.position;
        pos.y = originalY + amplification * Mathf.Sin (Time.time* frequency * Mathf.PI);

        transform.position = pos;
    }
}
